import requests

from src.utils.data_utils import APIRequestException


class HHAPIMapper:
    """
    Класс для работы с API HH.ru, предназначен для загрузки и обработки справочников
    профессиональных ролей и регионов.

    Attributes:
        role2parent_name (Dict[str, List[str]]): Словарь сопоставления названий
                                                        ролей и отраслей.
        role2parent_id (Dict[str, List[str]]): Словарь сопоставления ID ролей
                                            и отраслей.
        role_id2name (Dict[str, str]): Словарь сопоставления ID ролей и их названий.
        role_ids (List[str]): Список ID ролей.

        region2parent_id (Dict[str, str]): Словарь сопоставления ID регионов
                                        и их родителей.
        region_id2name (Dict[str, str]): Словарь сопоставления ID регионов
                                        и их названий.
        region_ids (List[str]): Список ID регионов.

    Methods:
        process_roles:
            Загружает и обрабатывает данные о профессиональных ролях с API HH.ru.
        process_areas:
            Загружает и обрабатывает данные о регионах с API HH.ru.
    """

    def __init__(self,
                 api_url_roles: str = 'https://api.hh.ru/professional_roles',
                 api_url_areas: str = 'https://api.hh.ru/areas',
                 country_id: int = 0):
        """Инициализация класса с пустыми справочниками и api url для справочников.

        Args:
            api_url_roles (str, optional): URL адрес API для загрузки данных.
                По умолчанию установлен на 'https://api.hh.ru/professional_roles'.
            api_url_areas (str, optional): URL адрес API для загрузки данных.
                По умолчанию установлен на 'https://api.hh.ru/areas'.
            country_id (int, optional): Идентификатор страны в ответе API,
                    для которой нужно загрузить данные о регионах.
                    По умолчанию 0, что соответствует Российской Федерации.
        """
        self.role2parent_name = {}
        self.role2parent_id = {}
        self.role_id2name = {}
        self.role_ids = []

        self.region2parent_id = {}
        self.region_id2name = {}
        self.region_ids = []

        self.api_url_roles = api_url_roles
        self.api_url_areas = api_url_areas
        self.country_id = country_id

    def process_roles(self):
        """
        Загружает и обрабатывает данные о профессиональных ролях с API HH.ru.

        Exceptions:
            APIRequestException: Возникает при ошибках HTTP запроса к API.
        """
        try:
            response = requests.get(self.api_url_roles, timeout=60)
            response.raise_for_status()
            json_prof_roles = response.json()

            categories_list = json_prof_roles['categories']

            for category in categories_list:
                parent_id, parent_name = category['id'], category['name']

                for role in category['roles']:
                    role_id, role_name = role['id'], role['name']

                    if role_id not in self.role_ids:
                        self.role_ids.append(role_id)
                    self.role_id2name[role_id] = role_name

                    if role_id not in self.role2parent_id:
                        self.role2parent_id[role_id] = []
                    if role_name not in self.role2parent_name:
                        self.role2parent_name[role_name] = []

                    self.role2parent_id[role_id].append(parent_id)
                    self.role2parent_name[role_name].append(parent_name)
        except requests.RequestException as e:
            raise APIRequestException(
                f"Ошибка при запросе к API регионов: {e}",
                status_code=response.status_code
            )

    def process_areas(self):
        """
        Загружает и обрабатывает данные о регионах с API HH.ru.

        Exceptions:
            APIRequestException: Возникает при ошибках HTTP запроса к API.
        """

        try:
            response = requests.get(self.api_url_areas, timeout=60)
            response.raise_for_status()
            json_areas = response.json()

            all_regions = json_areas[self.country_id]['areas']
            for region in all_regions:
                self.region_id2name[region['id']] = region['name']

                # Обработка для городов-регионов (Москва, СПБ)
                if len(region['areas']) == 0:
                    self.region2parent_id[region['id']] = region['id']
                else:
                    for locality in region['areas']:
                        self.region2parent_id[locality['id']] = locality['parent_id']
            self.region_ids = list(self.region_id2name.keys())
        except requests.RequestException as e:
            raise APIRequestException(
                f"Ошибка при запросе к API регионов: {e}",
                status_code=response.status_code
            )
