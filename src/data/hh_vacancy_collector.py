import logging
import time
from typing import Any, Dict, List, Optional

import requests
from tqdm import tqdm

from src.utils.data_utils import APIRequestException, extract_from_vacancy, format_dates


class VacancyCollector:
    """
    Класс-оркестратор для сбора данных о вакансиях.

    Methods:
    - get_json_vacancies: Получает данные о вакансиях из API и обрабатывает их.
    """

    def __init__(self,
                 per_page: int = 100,
                 max_page: int = 20,
                 threshold: int = 20,
                 base_url: str = 'https://api.hh.ru/vacancies',
                 timesleep_req: float = 0.35,
                 timesleep_error: int = 10,
                 max_attempts: int = 6):
        """
        Инициализация коллектора вакансий.

        Args:
            per_page (int): Количество вакансий на странице.
                            По умолчанию установлено на 100 (максимум).
            max_page (int): Максимальное количество страниц для обхода.
                            По умолчанию установлено на 20 (максимум).
            threshold (int): Максимальное число вакансий для определенной профессии
                             из конкретного региона. По умолчанию установлено на 20.
            base_url (str): Базовый URL API вакансий.
                            По умолчанию установлено https://api.hh.ru/vacancies.
            timesleep_req (float): Время задержки между запросами.
            timesleep_error (int): Время ожидания в секундах перед повторной попыткой
                                запроса в случае получения ошибки.
            max_attempts (int): Максимальное количество попыток запроса при ошибках.
        """
        self.per_page = per_page
        self.max_page = max_page
        self.threshold = threshold
        self.base_url = base_url
        self.timesleep_error = timesleep_error
        self.timesleep_req = timesleep_req
        self.max_attempts = max_attempts

    def get_json_vacancies(self,
                           region_ids: List[int],
                           role_ids: List[int],
                           date_from: int,
                           date_to: int) -> List[Dict[str, Any]]:
        """
        Получает данные о вакансиях из API и обрабатывает их.

        Args:
            region_ids (List[int]): Список идентификаторов регионов.
            role_ids (List[int]): Список идентификаторов профессиональных ролей.
            date_from (str): Строковое представление нач. даты в формате 'YYYY-MM-DD'.
            date_to (str): Строковое представление конеч. даты в формате 'YYYY-MM-DD'.

        Returns:
            List[Dict[str, Any]]: Список обработанных данных о вакансиях.
        """
        date_from, date_to = format_dates(date_from, date_to)
        logging.info(f'Сбор вакансий с {date_from} до {date_to}')

        all_json_vacancies = []

        with VacancyAPI(self.base_url, self.timesleep_error, self.max_attempts) as api:
            for region in tqdm(region_ids, desc='Regions'):
                for role in tqdm(role_ids, desc='Roles', leave=False):
                    processor = VacancyDataProcessor()
                    one_role_vacancies = []

                    for page in range(self.max_page):
                        params = api.build_params(date_from,
                                                  date_to,
                                                  region,
                                                  role,
                                                  page,
                                                  self.per_page)
                        data = api.fetch_vacancies(params)
                        time.sleep(self.timesleep_req)

                        if processor.is_data_empty_or_complete(data, page):
                            break
                        processor.add_without_duplicates(data['items'],
                                                         one_role_vacancies)

                        if len(one_role_vacancies) >= self.threshold:
                            break

                    all_json_vacancies.extend(one_role_vacancies[:self.threshold])

        logging.info(f'Всего собрано вакансий: {len(all_json_vacancies)}')
        return all_json_vacancies

class VacancyAPI:
    """
    Класс для взаимодействия с API вакансий.

    Отвечает за выполнение HTTP-запросов к API для получения данных о вакансиях.
    Использует сессии requests для улучшенной производительности.

    Attributes:
        session (requests.Session): Сессия для выполнения HTTP-запросов.
        base_url (str): Базовый URL API вакансий.
        timesleep_error (int): Время ожидания в секундах перед повторной попыткой
                               запроса в случае получения ошибки.
        max_attempts (int): Максимальное количество попыток выполнения запроса перед
                            прекращением попыток.

    Methods:
        fetch_vacancies:
            Выполняет запрос к API и возвращает данные. В случае возникновения
            исключения requests.RequestException c статус-кодом 403
            (ограничение доступа) ждет timesleep_error и повторяет попытку до достижения
            максимального количества попыток max_attempts.

        build_params: Строит и возвращает словарь параметров для запроса к API.
    """
    ERROR_403 = 403 # Ошибка, возникающая при слишком частых запросах API

    def __init__(self,
                 base_url: str,
                 timesleep_error: int,
                 max_attempts: int):
        """Инициализация сессии HTTP запросов и настройка параметров."""
        self.session = requests.Session()
        self.base_url = base_url
        self.timesleep_error = timesleep_error
        self.max_attempts = max_attempts

    def fetch_vacancies(self, params: Dict[str, Any]) -> Optional[Dict[str, Any]]:
        """
        Выполняет запрос к API и возвращает данные.

        Args:
            params (Dict[str, Any]): Параметры для запроса к API.

        Returns:
            Optional[Dict[str, Any]]: Ответ API в формате JSON или
                                      None при ошибке запроса.
        """
        attempts = 0
        while attempts < self.max_attempts:
            try:
                response = self.session.get(self.base_url, params=params)
                response.raise_for_status()
                return response.json()
            except requests.RequestException as e:
                attempts += 1

                if getattr(e, 'response', None) is not None \
                   and e.response.status_code == VacancyAPI.ERROR_403:
                    logging.warning("Доступ ограничен, ожидание повторной попыткой...")
                else:
                    logging.error(f"Ошибка при запросе к API: {e}")
                    if attempts >= self.max_attempts:
                        raise APIRequestException(f"Ошибка: {e}",)
                time.sleep(self.timesleep_error)

        logging.error("Превышено максимальное количество попыток запроса к API.")
        return None


    def build_params(self,
                     date_from: str,
                     date_to: str,
                     region: int,
                     role: int,
                     page: int,
                     per_page: int) -> Dict[str, Any]:
        """
        Строит параметры для запроса к API.

        Args:
            date_from (str): Начальная дата.
            date_to (str): Конечная дата.
            region (int): Идентификатор региона.
            role (int): Идентификатор профессиональной роли.
            page (int): Номер страницы.
            per_page (int): Количество вакансий на странице.

        Returns:
            Dict[str, Any]: Словарь параметров для запроса.
        """

        return {
            'date_from': date_from,
            'date_to': date_to,
            'area': region,
            'page': page,
            'per_page': per_page,
            'only_with_salary': 'true',
            'professional_role': role
        }

    def __enter__(self):
        """Возвращаем объект при входе в контекст."""
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Закрытие сессии при выходе из контекста."""
        self.session.close()

class VacancyDataProcessor:
    """
    Класс для первичной обработки и анализа данных о вакансиях.
    Удаляет дубликаты вакансий и проверяет данные, полученные от API.

    Attributes:
        unique_vacancies (Set): Множество уникальных ключей вакансий.

    Methods:
        add_without_duplicates(vacancies: List[Dict[str, Any]],
                               one_role_vacancies: List[Dict[str, Any]]):
        Добавляет вакансии без дубликатов в список.

        is_data_empty_or_complete(data: Dict[str, Any], page: int):
        Проверяет, пустые ли данные или достигнут конец страниц.
    """

    def __init__(self):
        """
        Инициализация обработчика данных вакансий.

        Args:
            threshold (int): Пороговое значение для обработки вакансий.
        """
        self.unique_vacancies = set()


    def _create_unique_key(self, vacancy: Dict[str, Any]):
        """ Создает уникальный ключ для вакансии."""
        return (
            extract_from_vacancy(vacancy, 'name'),
            extract_from_vacancy(vacancy, 'employer', 'name'),
            extract_from_vacancy(vacancy, 'salary', 'from'),
            extract_from_vacancy(vacancy, 'salary', 'to'),
            extract_from_vacancy(vacancy, 'snippet', 'responsibility'),
            extract_from_vacancy(vacancy, 'professional_roles', 0, 'id')
        )

    def add_without_duplicates(self,
                               vacancies: List[Dict[str, Any]],
                               one_role_vacancies: List[Dict[str, Any]]):
        """
        Добавляет вакансии без дубликатов в список.

        Args:
            vacancies (List[Dict[str, Any]]): Список вакансий для проверки.
            one_role_vacancies (List[Dict[str, Any]]): Список вакансий без дубликатов.
        """
        for vacancy in vacancies:
            unique_key = self._create_unique_key(vacancy)
            if unique_key not in self.unique_vacancies:
                one_role_vacancies.append(vacancy)
                self.unique_vacancies.add(unique_key)

    def is_data_empty_or_complete(self, data: Dict[str, Any], page: int):
        """
        Проверяет, пустые ли данные или достигнут конец страниц.

        Args:
            data (Dict[str, Any]): Данные, полученные от API.
            page (int): Текущий номер страницы.

        Returns:
            bool: Возвращает True, если данные пусты или страницы закончились.
        """
        return not data or \
               len(data.get('items', [])) == 0 or \
               page >= data.get('pages', 0)
