import logging
import time
from typing import Any, Dict, List

import pandas as pd
import requests
from tqdm import tqdm

from src.utils.data_utils import APIRequestException, extract_from_vacancy


class VacancyPreprocessor:
    """
    Класс для предобработки информации о вакансиях.

    Attributes:
        mapping_dicts (Dict[str, Any]): Словарь сопоставлений для ID и названий
                                        регионов, ролей и отраслей.
        timesleep_req (float): Время задержки между успешными запросами.
        timesleep_error (int): Время задержки между повторными запросами при ошибке.
        max_attempts (int): Максимальное количество попыток запроса при ошибках.

    Methods:
        preprocess(json_vacancies: List[Dict[str, Any]]) -> pd.DataFrame:
            Производит полную предобработку списка вакансий, полученных в формате JSON.
            Получает полные данные о каждой вакансии и преобразует их
            в формат DataFrame.

        base_transform(vacancies: List[Dict[str, Any]]) -> pd.DataFrame:
            Преобразует список вакансий в DataFrame, форматируя и добавляя необходимые
            поля.
            Извлекает и преобразует данные, такие как навыки, роли,
            зарплата и информация о городах, используя сопоставления из mapping_dicts.

        get_full_vacancy(vacancies: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
            Получает полные данные о каждой вакансии из списка. В случае ошибок запроса,
            делает повторные попытки до достижения максимального числа попыток.
            Может генерировать APIRequestException при неудачных попытках.
    """
    ERROR_403 = 403 # Ошибка, возникающая при слишком частых запросах API
    ERROR_404 = 404 # Ошибка, возникающая при отсутствии вакансии с текущ. url на сайте
    def __init__(self,
                 mapping_dicts: Dict[str, Any],
                 timesleep_req: float = 0.35,
                 timesleep_error: int = 2,
                 max_attempts: int = 5) -> None:
        """
        Инициализирует VacancyPreprocessor с заданными параметрами.

        Args:
            mapping_dicts (Dict[str, Any]): Словарь сопоставлений.
                Ожидается, что этот словарь будет содержать следующие ключи:
                - 'region2parent_id': Сопоставление ID городов и регионов.
                - 'role2parent_name': Сопоставление названий ролей и отраслей.
                - 'role2parent_id': Сопоставление ID ролей и отраслей.
            timesleep_req (float): Время задержки между запросами.
            timesleep_error (int): Время задержки между повторными запросами при ошибке.
            max_attempts (int): Максимальное количество попыток запроса при ошибках.
        """
        self.mapping_dicts = mapping_dicts
        self.timesleep_req = timesleep_req
        self.timesleep_error = timesleep_error
        self.max_attempts = max_attempts

    def preprocess(self, json_vacancies: List[Dict[str, Any]]) -> pd.DataFrame:
        """
        Производит полную предобработку списка вакансий.

        Args:
            json_vacancies (List[Dict[str, Any]]): Список вакансий в формате JSON.

        Returns:
            pd.DataFrame: DataFrame, содержащий предобработанные данные о вакансиях.
        """
        full_vacancies = self.get_full_vacancy(json_vacancies)
        df_vacancies = self.base_transform(full_vacancies)
        return df_vacancies

    def get_full_vacancy(self, vacancies: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
        """
        Получает полные данные о каждой вакансии из списка.

        Args:
            vacancies (List[Dict[str, Any]]): Список вакансий с базовой информацией.

        Returns:
            List[Dict[str, Any]]: Список полных данных о вакансиях.
        """
        full_vacancies = []
        with requests.Session() as session:
            for vacancy in tqdm(vacancies, leave=False, desc="Processing Vacancies"):
                attempts = 0
                while attempts < self.max_attempts:
                    try:
                        response = session.get(vacancy['url'])
                        response.raise_for_status()
                        full_vacancy = response.json()
                        full_vacancies.append(full_vacancy)
                        time.sleep(self.timesleep_req)
                        break  # Успешный запрос, прекращаем повторы
                    except requests.RequestException as e:
                        attempts += 1
                        if getattr(e, 'response', None) is not None:
                            if e.response.status_code == VacancyPreprocessor.ERROR_403:
                                logging.warning(
                                    "Доступ ограничен, ожидание повторной попытки..."
                                )
                            elif \
                                e.response.status_code == VacancyPreprocessor.ERROR_404:
                                logging.warning(f"Вакансия {vacancy['url']} не найдена")
                                logging.info("Переход к обработке следующей вакансии")
                                break
                        if attempts >= self.max_attempts:
                            logging.warning('''max_attempts повторных запросов к API''')
                            logging.error(f"Ошибка при запросе к API вакансий: {e}")
                            raise APIRequestException(f"Ошибка: {e}")
                        time.sleep(self.timesleep_error)
        return full_vacancies

    def base_transform(self, vacancies: List[Dict[str, Any]]) -> pd.DataFrame:
        """
        Преобразует список вакансий в DataFrame с определенными полями.

        Args:
            vacancies (List[Dict[str, Any]]): Список вакансий.

        Returns:
            pd.DataFrame: DataFrame, содержащий обработанную информацию о вакансиях.
        """
        data = []
        for vacancy in vacancies:
            key_skills = extract_from_vacancy(vacancy, 'key_skills')

            vacancy_data = {
                'id': extract_from_vacancy(vacancy, 'id'),
                'custom_position': extract_from_vacancy(vacancy, 'name'),
                'description': extract_from_vacancy(vacancy, 'description'),
                'schedule': extract_from_vacancy(vacancy, 'schedule', 'name'),
                'experience': extract_from_vacancy(vacancy, 'experience', 'name'),
                'employment': extract_from_vacancy(vacancy, 'employment', 'name'),
                'salary_from': extract_from_vacancy(vacancy, 'salary', 'from'),
                'salary_to': extract_from_vacancy(vacancy, 'salary', 'to'),
                'salary_pay_type': extract_from_vacancy(vacancy, 'salary', 'gross'),
                'city_name': extract_from_vacancy(vacancy, 'area', 'name'),
                'city_id': extract_from_vacancy(vacancy, 'area', 'id'),
                'published_date': extract_from_vacancy(vacancy, 'published_at'),
                'work_skills': [skill['name'] for skill in key_skills],
                'professional_roles': extract_from_vacancy(
                    vacancy, 'professional_roles', 0, 'name'
                ),
                'professional_roles_id': extract_from_vacancy(
                    vacancy, 'professional_roles', 0, 'id'
                )
            }
            data.append(vacancy_data)

        df = pd.DataFrame(data)
        df['region_id'] = df['city_id'].map(
            self.mapping_dicts.region2parent_id
        )
        df['industry_name'] = df['professional_roles'].map(
            self.mapping_dicts.role2parent_name
        )
        df['industry_id'] = df['professional_roles_id'].map(
            self.mapping_dicts.role2parent_id
        )

        return df
