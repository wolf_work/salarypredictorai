import argparse
import logging
from typing import Dict, Tuple

from src.data.hh_data_mapper import HHAPIMapper
from src.data.hh_vacancy_collector import VacancyCollector
from src.data.hh_vacancy_preprocessor import VacancyPreprocessor
from src.utils.data_utils import (
    batch_generator,
    load_csv_from_folder,
    load_json_from_folder,
    remove_directory,
)
from src.utils.pipeline_utils import (
    load_config,
    process_regions_and_save_json,
    process_vacancies_and_save_csv,
    setup_logging,
)


def parse_arguments() -> Tuple[str, str]:
    """
    Разбирает аргументы командной строки и возвращает их.

    Returns:
        Tuple[str, str]: Возвращает путь к файлу конфигурации и выбранный этап запуска.
    """
    parser = argparse.ArgumentParser(
        description="Скрипт для сбора и обработки данных о вакансиях"
    )
    parser.add_argument('-config_path',
                        type=str,
                        default='src/configs/settings.yaml',
                        help="Путь к файлу конфигурации")
    parser.add_argument('--start',
                        type=str,
                        choices=['all', 'json', 'csv'],
                        default='all',
                        help="Этап, с которого начинается исполнение скрипта")
    args = parser.parse_args()
    return args.config_path, args.start


def init_config_and_logging(config_path: str) -> Dict:
    """
    Инициализирует конфигурацию и настраивает логирование.

    Загружает конфигурационные данные из указанного файла и настраивает параметры
    логирования на основе полученной конфигурации.

    Args:
        config_path (str): Путь к файлу конфигурации.

    Returns:
        Dict: Словарь с загруженной конфигурацией.
    """
    cfg = load_config(config_path)
    setup_logging(cfg['log_dir'])
    return cfg


def init_components(cfg: Dict) -> Tuple[HHAPIMapper,
                                        VacancyCollector,
                                        VacancyPreprocessor,
                                        Dict]:
    """
    Инициализирует компоненты для сбора и обработки данных о вакансиях.

    Args:
        cfg (Dict): Конфигурация скрипта.

    Returns:
        Tuple[HHAPIMapper, VacancyCollector, VacancyPreprocessor, Dict]:
        Возвращает инициализированные объекты и словарь с путями.
    """
    mapper = HHAPIMapper()
    mapper.process_areas()
    mapper.process_roles()

    vac_collector = VacancyCollector(per_page=cfg['per_page'],
                                     max_page=cfg['max_page'],
                                     threshold=cfg['threshold'],
                                     base_url=cfg['base_url'],
                                     timesleep_req=cfg['timesleep_req'],
                                     timesleep_error=cfg['timesleep_error'],
                                     max_attempts=cfg['max_attempts'])
    vac_preprocessor = VacancyPreprocessor(mapper,
                                           timesleep_error=cfg['timesleep_error'],
                                           timesleep_req=cfg['timesleep_req'],
                                           max_attempts=cfg['max_attempts'])

    paths = {
        'json_dir': f"data/raw/data_json_{cfg['date_from']}-{cfg['date_to']}",
        'csv_dir': f"data/raw/data_csv_{cfg['date_from']}-{cfg['date_to']}",
        'final_csv': \
            f"data/interim/data_hh/vacancies_{cfg['date_from']}-{cfg['date_to']}.csv"
    }

    return mapper, vac_collector, vac_preprocessor, paths


def extract_basic_vacancy_info(cfg: Dict,
                               mapper: HHAPIMapper,
                               vac_collector: VacancyCollector,
                               path_to_json_dir: str) -> None:
    """
    Извлекает базовую информацию о вакансиях для каждого региона и каждой профессии,
    сохраняя результаты в формате JSON.
    Список со всеми регионами делятся на батчи размером cfg['batch_size_extract'],
    извлеченные вакансии в формате JSON сохраняются для каждого батча в директорию
    path_to_json_dir.

    При некорректном завершении работы, обработку можно продолжить, присвоив
    параметру в конфиге cfg['start_batch_extract'] требуемый номер батча, с которого
    необходимо продолжать обработку.

    Args:
        cfg: Конфигурация скрипта.
        mapper: Объект HHAPIMapper для работы с API HH.
        vac_collector: Объект VacancyCollector для сбора данных о вакансиях.
        path_to_json_dir: Путь к директории для сохранения JSON файлов.
    """
    logging.info("START extract_basic_vacancy_info()")
    region_ids = \
        cfg['filter_region_ids'] if cfg['filter_region_ids'] else mapper.region_ids
    role_ids = cfg['filter_role_ids'] if cfg['filter_role_ids'] else mapper.role_ids

    regions_batches = batch_generator(region_ids,
                                      cfg['batch_size_extract'],
                                      cfg['start_batch_extract'])
    process_regions_and_save_json(regions_batches,
                                  role_ids,
                                  vac_collector,
                                  cfg['date_from'],
                                  cfg['date_to'],
                                  path_to_json_dir,
                                  cfg['start_batch_process'])
    logging.info("END extract_basic_vacancy_info()")


def process_full_vacancy_info(cfg: Dict,
                              vac_preprocessor: VacancyPreprocessor,
                              path_to_json_dir: str,
                              path_to_csv_dir: str) -> None:
    """
    Получает и предобрабатывает полную информацию о вакансиях, загружая данные из
    полученных с помощью extract_basic_vacancy_info() файлов JSON, обработка ведется
    батчами с размером cfg['batch_size_process'], промежуточные резульаты обработки
    каждого батча сохраняются в формате CSV в директорию path_to_csv_dir.

    При некорректном завершении работы, обработку можно продолжить, присвоив
    параметру в конфиге cfg['start_batch_process'] требуемый номер батча, с которого
    необходимо продолжать обработку.

    Args:
        cfg: Конфигурация скрипта.
        vac_preprocessor: Объект VacancyPreprocessor для предобработки
                          данных о вакансиях.
        path_to_json_dir: Путь к директории с JSON файлами.
        path_to_csv_dir: Путь к директории для сохранения CSV файлов.
    """
    logging.info('START process_full_vacancy_info')
    all_json_vacancies = load_json_from_folder(path_to_json_dir)

    vacancies_batches = batch_generator(all_json_vacancies,
                                        cfg['batch_size_process'],
                                        cfg['start_batch_process'])
    process_vacancies_and_save_csv(vacancies_batches,
                                   vac_preprocessor,
                                   path_to_csv_dir,
                                   cfg['start_batch_process'])
    logging.info('END process_full_vacancy_info')


def finalize(cfg: Dict,
             path_to_json_dir: str,
             path_to_csv_dir: str,
             path_to_result_csv: str) -> None:
    """
    Финализирует процесс обработки данных: собирает все данные из CSV файлов
    в директории path_to_csv_dir в один DataFrame и сохраняет их,
    очищает временные директории path_to_json_dir и path_to_csv_dir, если это
    предусмотрено конфигурацией.

    Args:
        cfg: Конфигурация скрипта.
        path_to_json_dir: Путь к директории с JSON файлами.
        path_to_csv_dir: Путь к директории с CSV файлами.
        path_to_result_csv: Путь к итоговому CSV файлу.
    """
    logging.info('START finalize')
    final_df = load_csv_from_folder(path_to_csv_dir)
    final_df.to_csv(path_to_result_csv, index=False)
    if cfg['remove_raw_dirs']:
        remove_directory(path_to_json_dir)
        remove_directory(path_to_csv_dir)
    logging.info('END finalize')


def main(config_path, start_from):
    """
    Основная функция для запуска процесса сбора и обработки данных о вакансиях.

    Args:
        config_path (str): Путь к файлу конфигурации.
        start_from (str): Этап, с которого начинается исполнение скрипта.
    """
    # Инициализация конфига, логгера и компонентов
    cfg = init_config_and_logging(config_path)
    mapper, vac_collector, vac_preproc, paths = init_components(cfg)

    # Извлечение базовой информации о вакансиях
    if start_from in ['json', 'all']:
        extract_basic_vacancy_info(cfg, mapper, vac_collector, paths['json_dir'])

    # Извлечение полной информации и предобработка
    if start_from in ['csv', 'all']:
        process_full_vacancy_info(cfg, vac_preproc, paths['json_dir'], paths['csv_dir'])

    # Сохранение итогового датасета и удаление временных директорий (опционально)
    finalize(cfg, paths['json_dir'], paths['csv_dir'], paths['final_csv'])


if __name__ == "__main__":
    config_path, start_from = parse_arguments()
    main(config_path, start_from)
