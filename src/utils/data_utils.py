import json
import logging
import os
import shutil
from datetime import datetime
from typing import Any, Iterable, List

import numpy as np
import pandas as pd


class APIRequestException(Exception):
    """
    Исключение, возникающее при ошибках запросов к API.

    Atributes:
        message (str): описание ошибки.
        status_code (int, optional): HTTP статус код ошибки, если доступен.
    """
    def __init__(self, message: str, status_code: int = None):
        super().__init__(message)
        self.message = message
        self.status_code = status_code

    def __str__(self):
        """
        Возвращает строковое представление исключения.
        """
        if self.status_code:
            return f"[{self.status_code}] {self.message}"
        return self.message

def format_dates(date_from: str, date_to: str):
    """
    Функция для форматирования дат в формат ISO8601.

    Args:
    date_from (str): Строковое представление начальной даты в формате 'YYYY-MM-DD'.
    date_to (str): Строковое представление конечной даты в формате 'YYYY-MM-DD'.

    Returns:
    tuple: кортеж, содержащий строковые представления начальной и конечной даты
           в формате ISO8601.
    """
    start_datetime = datetime.strptime(date_from, '%Y-%m-%d').date()
    end_datetime = datetime.strptime(date_to, '%Y-%m-%d').date()

    formatted_date_from = start_datetime.strftime('%Y-%m-%dT00:00:00Z')
    formatted_date_to = end_datetime.strftime('%Y-%m-%dT23:59:59Z')
    return formatted_date_from, formatted_date_to


def extract_from_vacancy(vacancy, *keys, default=np.nan):
    """
    Функция для получения значения из вакансии по цепочке ключей или возвращения
    значения по умолчанию.

    Args:
    vacancy (dict): Словарь, содержащий данные о вакансии.
    *keys: Переменное количество аргументов, ключей словаря, которые определяют путь
        к извлекаемому значению.
    default: Значение, возвращаемое, если ключ отсутствует в словаре.
        По умолчанию np.nan.

    Returns:
    Значение, найденное по указанной цепочке ключей, или значение по умолчанию,
    если ключ отсутствует.
    """
    try:
        for key in keys:
            vacancy = vacancy[key]
        return vacancy
    except (KeyError, IndexError, TypeError):
        return default

def batch_generator(iterable: Iterable,
                    batch_size: int,
                    start_batch: int = 0) -> List[List[Any]]:
    """
    Генератор, который создает батчи заданного размера из переданного итерируемого
    объекта, начиная с заданного номера батча.

    Args:
        iterable (Iterable): Итерируемый объект.
        batch_size (int): Размер батча.
        start_batch (int): Номер батча, с которого начинается генерация.

    Yields:
        List[Any]: Батч заданного размера.
    """
    current_batch = 0
    batch = []
    for item in iterable:
        batch.append(item)
        if len(batch) == batch_size:
            if current_batch >= start_batch:
                yield batch
            batch = []
            current_batch += 1
    if batch and current_batch >= start_batch:
        yield batch

def save_json(data, filename: str):
    """Сохранение данных в JSON-файл."""
    with open(filename, 'w') as file:
        json.dump(data, file)

def load_json_from_folder(path: str):
    """Загрузка всех JSON-файлов из папки."""
    all_data = []
    for filename in os.listdir(path):
        if filename.endswith('.json'):
            with open(os.path.join(path, filename), 'r') as file:
                all_data.extend(json.load(file))
    return all_data

def load_csv_from_folder(path: str):
    """Загрузка всех CSV-файлов из папки в DataFrame."""
    df_list = [pd.read_csv(os.path.join(path, filename)) \
               for filename in os.listdir(path) if filename.endswith('.csv')]
    return pd.concat(df_list, ignore_index=True)

def remove_directory(path: str):
    """
    Удаляет указанную директорию вместе с ее содержимым.

    Args:
        path (str): Путь к директории, которую необходимо удалить.
    """
    if os.path.exists(path) and os.path.isdir(path):
        shutil.rmtree(path)
        logging.info(f"Директория '{path}' успешно удалена.")
    else:
        logging.warning(f"Директория '{path}' не найдена.")
