import logging
import os
from datetime import datetime
from typing import Any, Dict, Iterable, List

import yaml

from src.data.hh_vacancy_collector import VacancyCollector
from src.data.hh_vacancy_preprocessor import VacancyPreprocessor
from src.utils.data_utils import save_json


def process_regions_and_save_json(regions_batches: Iterable[List[int]],
                                  roles_ids: List[int],
                                  collector: VacancyCollector,
                                  date_from: str,
                                  date_to: str,
                                  path_to_save: str,
                                  start_batch: int):
    """
    Обрабатывает батчи регионов, собирает данные о вакансиях и сохраняет их в JSON.

    Args:
        regions_batches (Iterable[List[int]]): Итерируемый объект батчей регионов.
        roles_ids (List[int]): Список идентификаторов профессиональных ролей.
        collector (VacancyCollector): Экземпляр класса VacancyCollector
                                      для сбора данных.
        date_from (str): Строковое представление нач. даты в формате 'YYYY-MM-DD'.
        date_to (str): Строковое представление конеч. даты в формате 'YYYY-MM-DD'.
        path_to_save (str): Путь к папке для сохранения результатов.
        start_batch (int): Номер батча, с которого начинается обработка.
    """
    if not os.path.exists(path_to_save):
        os.makedirs(path_to_save)

    for i, region_batch in enumerate(regions_batches, start_batch):
        logging.info(f'START process {i}-batch json')
        json_vacancies = collector.get_json_vacancies(region_batch,
                                                      roles_ids,
                                                      date_from,
                                                      date_to)
        save_json(json_vacancies, os.path.join(path_to_save, f'batch_{i}.json'))
        logging.info(f'SAVE process {i}-batch json')

def process_vacancies_and_save_csv(vacancies_batches: Iterable[List[Dict[str, Any]]],
                                   preprocessor: VacancyPreprocessor,
                                   path_to_save: str,
                                   start_batch: int):
    """
    Обрабатывает батчи вакансий и сохраняет результаты в CSV-файлах.

    Args:
        vacancies_batches (Iterable[List[Dict[str, Any]]]): Итерируемый объект
                                                            батчей вакансий.
        preprocessor (VacancyPreprocessor): Экземпляр класса VacancyPreprocessor
                                            для предобработки данных.
        path_to_save (str): Путь к папке для сохранения результатов в формате CSV.
        start_batch (int): Номер батча, с которого начинается обработка.
    """
    if not os.path.exists(path_to_save):
        os.makedirs(path_to_save)

    for i, vacancy_batch in enumerate(vacancies_batches, start_batch):
        logging.info(f'START process {i}-batch csv')
        df = preprocessor.preprocess(vacancy_batch)
        df.to_csv(os.path.join(path_to_save, f'batch_{i}.csv'), index=False)
        logging.info(f'SAVE process {i}-batch csv')

def load_config(config_path):
    with open(config_path, 'r') as file:
        return yaml.safe_load(file)

def setup_logging(log_directory):
    if not os.path.exists(log_directory):
        os.makedirs(log_directory)

    current_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    log_filename = f"{log_directory}/log_{current_time}.log"

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(levelname)s - %(message)s',
                        handlers=[
                            logging.FileHandler(log_filename),
                            logging.StreamHandler()
                        ])
